﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameProperties))]
public class GamePropertiesEditor : Editor
{
    GameProperties myScript;

    private void OnEnable() {
        myScript = (GameProperties)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if(GUILayout.Button("Reset Properties")){
            Debug.Log("Resetou");
        }
    }
}
