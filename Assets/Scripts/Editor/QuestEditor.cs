﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Quest))]
public class QuestEditor : Editor
{
    private Quest quest;
    
    private void OnEnable() 
    {
        quest = (Quest)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GUILayout.BeginHorizontal();
        if(GUILayout.Button("Ativar Quest")){
            quest.SetQuestActive();
        }
        if(GUILayout.Button("Resetar Todos os Objetivos")){
            quest.ResetAllObjectives();
        }
        GUILayout.EndHorizontal();

        foreach (var objective in quest.objectives)
        {
            GUILayout.BeginHorizontal();
            GUI.enabled = false;
            EditorGUILayout.Toggle(objective.ObjectiveHasCompleted,GUILayout.Width(20f));
            EditorGUILayout.TextField(objective.objectiveName);
            GUI.enabled = true;
            GUILayout.EndHorizontal();
        }
    }
}
