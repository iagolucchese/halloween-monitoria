﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(QuestManager))]
public class QuestManagerEditor : Editor
{
    public const string QuestsFolder = "Assets/Resources/Quests/";
    private QuestManager manager;

    private void OnEnable() 
    {
        manager = (QuestManager)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GUILayout.BeginHorizontal();

        if(GUILayout.Button("Carregar Quests da pasta")){
            manager.LoadQuestsFromFolder();
        }
        if(GUILayout.Button("Criar Nova Quest")){
            CreateNewQuest();
        }

        GUILayout.EndHorizontal();
    }

    public void CreateNewQuest()
    {
        Quest asset = ScriptableObject.CreateInstance<Quest>();
        asset.questName = "NovaQuest";
        
        AssetDatabase.CreateAsset(asset, QuestsFolder + "NovaQuest.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;

        manager.AllQuestsList.Add(asset);
    }
}
