﻿ using System;
 using UnityEditor;
 
 [CustomEditor(typeof(QuestTrigger)), CanEditMultipleObjects]
 public class QuestTriggerEditor : Editor 
 {
     public SerializedProperty
        triggerName,
        isActive,
        firesMoreThanOnce,
        hasAlreadyFired,
        canBeUntriggered,
        triggerType,
        shouldTrigger,
        checkForTag,
        doCountdown,
        timeToWait,
        onTriggerActivatedEvent,
        onTriggerRevertedEvent;
     
     private void OnEnable ()
     {
        // Setup the SerializedProperties
        triggerName = serializedObject.FindProperty ("triggerName");
        isActive = serializedObject.FindProperty ("isActive");
        firesMoreThanOnce = serializedObject.FindProperty ("firesMoreThanOnce");
        hasAlreadyFired = serializedObject.FindProperty ("hasAlreadyFired");
        canBeUntriggered = serializedObject.FindProperty("canBeUntriggered");
        triggerType = serializedObject.FindProperty ("triggerType");
        shouldTrigger = serializedObject.FindProperty("shouldTrigger");
        checkForTag = serializedObject.FindProperty("checkForTag");
        doCountdown = serializedObject.FindProperty("doCountdown");
        timeToWait = serializedObject.FindProperty("timeToWait");
        onTriggerActivatedEvent = serializedObject.FindProperty("onTriggerActivatedEvent");
        onTriggerRevertedEvent = serializedObject.FindProperty("onTriggerRevertedEvent");
     }
     
     public override void OnInspectorGUI() 
     {
        serializedObject.Update ();
        EditorGUILayout.PropertyField(triggerName);
        EditorGUILayout.PropertyField(isActive);
        EditorGUILayout.PropertyField(firesMoreThanOnce);
        EditorGUILayout.PropertyField(hasAlreadyFired);

        EditorGUILayout.PropertyField(triggerType);
        QuestTrigger.TriggerTypes tt = (QuestTrigger.TriggerTypes)triggerType.enumValueIndex;
        
        switch(tt) 
        {
            case QuestTrigger.TriggerTypes.Collision:
                EditorGUILayout.PropertyField(canBeUntriggered);
                //EditorGUILayout.TextArea("Trigger por posição requer um collider no objeto, e pode opcionalmente checar por uma tag.");
                EditorGUILayout.PropertyField(checkForTag);
                //EditorGUILayout.IntSlider ( valForA_Prop, 0, 10, new GUIContent("valForA") );
                break;

            case QuestTrigger.TriggerTypes.Time:
                EditorGUILayout.PropertyField(doCountdown);
                EditorGUILayout.PropertyField(timeToWait);
                /* EditorGUILayout.PropertyField( controllable_Prop, new GUIContent("controllable") );    
                EditorGUILayout.IntSlider ( shouldTrigger, 0, 100, new GUIContent("valForAB") ); */
                break;

            case QuestTrigger.TriggerTypes.Quest:
                break;
                
            case QuestTrigger.TriggerTypes.Boolean:            
                EditorGUILayout.PropertyField(shouldTrigger);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        EditorGUILayout.PropertyField(onTriggerActivatedEvent);
        if (canBeUntriggered.boolValue)
            EditorGUILayout.PropertyField(onTriggerRevertedEvent);
        serializedObject.ApplyModifiedProperties();
     }
 }