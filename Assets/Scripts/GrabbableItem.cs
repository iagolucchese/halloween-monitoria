﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class GrabbableItem : MonoBehaviour
{
    [SerializeField] private bool canBeGrabbed;
    public bool CanBeGrabbed
    {
        get => canBeGrabbed;
        set => canBeGrabbed = value;
    }

    public Material defaultMaterial;
    public Material outlineMaterial;
    
    private new Rigidbody rigidbody;
    private new Collider collider;
    private MeshRenderer mesh;
    
    private void Awake()
    {
        rigidbody = GetComponentInChildren<Rigidbody>();
        collider = GetComponent<Collider>();
        mesh = GetComponent<MeshRenderer>();
        if (collider == null)
            Debug.LogError(gameObject.name + " is a grabbable item without a collider");
    }

    public void ToggleOutline(bool on)
    {
        mesh.material = on ? outlineMaterial : defaultMaterial;
    }

    public void Pickup(Transform parent = null)
    {
        if (parent)
        {
            transform.parent = parent;
            transform.localPosition = Vector3.zero;
        }

        TogglePhysicsAndInteraction(false);
    }

    public void Drop()
    {
        transform.parent = null;
        TogglePhysicsAndInteraction(true);
    }
    
    public void Throw(Vector3 throwForce, Vector3 forwardDirection)
    {
        transform.parent = null;
        TogglePhysicsAndInteraction(true);
        
        Physics.IgnoreLayerCollision(gameObject.layer, PlayerBehavior.Instance.gameObject.layer);
        //Physics.IgnoreCollision(gameObject.GetComponent<Collider>(), PlayerBehavior.Instance.GetComponent<Collider>(), true);
        
        rigidbody.AddForce(
            throwForce.z * forwardDirection, 
            ForceMode.Force
            );
        rigidbody.AddForce(
            throwForce.y * Vector3.up, 
            ForceMode.Force
        );
    }

    private void TogglePhysicsAndInteraction(bool on)
    {
        rigidbody.useGravity = on;
        rigidbody.isKinematic = !on;
        collider.enabled = on;
    }
}
