﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class InteractableBehavior : MonoBehaviour
{
    public bool playerIsClose;
    public bool canBeInteractedWith = true;
    public bool canOnlyInteractOnce = true;
    public bool hasBeenInteractedWith;
    public UnityEvent interactEvent;

    private InputManager input;

    private void Start()
    {
        input = InputManager.Instance;
    }

    public void DoInteract()
    {
        if(canOnlyInteractOnce && hasBeenInteractedWith)
            return;
        hasBeenInteractedWith = true;
        interactEvent.Invoke();        
    }

    private void Update()
    {
        if (!playerIsClose || !canBeInteractedWith) return;
        if (input.useDown)
            DoInteract();
    }

    private void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerIsClose = true;
        }
    }

    private void OnTriggerExit(Collider other) 
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerIsClose = false;
        }
    }    
}
