﻿using System.Linq;
using UnityEngine;

public class GroundCollisionDetector : MonoBehaviour
{
    private const string CheckForTag = "Ground";

    public float checkSphereRadius = 0.5f;
    public bool debugShowSphere;
    
    private PlayerMovementController playerMovementController;
    
    private void Start()
    {
        playerMovementController = PlayerBehavior.Instance.PlayerMovement;
    }

    private void Update()
    {
        var colliders = Physics.OverlapSphere(transform.position, checkSphereRadius);
        if (colliders.Any(col => col.CompareTag(CheckForTag)))
        {
            playerMovementController.isGrounded = true;
            return;
        }
        playerMovementController.isGrounded = false;
    }
    
    private void OnDrawGizmosSelected()
    {
        if (debugShowSphere)
            Gizmos.DrawSphere(transform.position, checkSphereRadius);
    }
}
