﻿using UnityEngine;

public class PlayerAnimationFunctions : MonoBehaviour
{
    private PlayerBehavior playerBehavior;
    
    private void Start()
    {
        playerBehavior = PlayerBehavior.Instance;
    }
    
    public void DoGrab()
    {
        playerBehavior.AnimatorGrabPoint = true;
    }

    public void DoThrow()
    {
        playerBehavior.AnimatorThrowPoint = true;
    }
}
