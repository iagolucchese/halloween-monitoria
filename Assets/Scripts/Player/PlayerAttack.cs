﻿using System.Collections;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private Transform heldWeaponSpot;
    [SerializeField] private Weapon selectedWeapon;
    [SerializeField] private WeaponProjectile weaponObjectInHand = null;
    
    public int attackBlock;
    public bool isAttacking;
    private int selectedWeaponNumber;
    
    //properties
    public int SelectedWeaponNumber
    {
        get => selectedWeaponNumber;
        private set
        {
            //cycle from 0 to max, and vice versa
            selectedWeaponNumber = value >= NumberOfWeapons ? 0 : value < 0 ? NumberOfWeapons - 1 : value;
            
            selectedWeapon = playerBehavior.WeaponFromIndex(selectedWeaponNumber);
            PlayerBehavior.Instance.OnWeaponChanged(selectedWeaponNumber);
            UpdateWeaponObjectOnHand();
        }
    }
    private int NumberOfWeapons => playerBehavior.listOfWeapons.Count;
    private bool IsBlockedFromAttacking => attackBlock > 0;
    private bool WeaponIsEmptyHand => SelectedWeaponNumber == 0;
    private bool IsAiming => playerBehavior.PlayerGrabThrow.isAiming;
    
    private InputManager input;
    private PlayerBehavior playerBehavior;
    
    //coroutine
    private WaitForEndOfFrame waitForEndOfFrame;
    private Coroutine attackCoroutine;
    
    private void Start()
    {
        waitForEndOfFrame = new WaitForEndOfFrame();
        
        input = InputManager.Instance;
        playerBehavior = PlayerBehavior.Instance;
        
        //começa com a arma zero (emptyHand)
        SelectedWeaponNumber = 0;

        if (heldWeaponSpot != null) return;
        heldWeaponSpot = transform;
        Debug.Log("Weapon held spot is null, using this transform: " + name);
    }
    private void Update()
    {
        CycleWeaponInputs();
        SolveAttacking();
    }
    
    private void CycleWeaponInputs()
    {
        if (input.cycleLeftDown)
            SelectedWeaponNumber--;
        else if (input.cycleRightDown)
            SelectedWeaponNumber++;
    }

    private void UpdateWeaponObjectOnHand()
    {
        //se ja tem um objeto na mão, destrua ele
        if (weaponObjectInHand != null)
        {
            Destroy(weaponObjectInHand.gameObject);
            weaponObjectInHand = null;
        }
        //se a arma selecionada tem um prefab de projetil, cria e poe ele na mão
        if (selectedWeapon.projectilePrefab != null)
        {
            weaponObjectInHand = 
                Instantiate(selectedWeapon.projectilePrefab, heldWeaponSpot.position, Quaternion.identity)
                .GetComponent<WeaponProjectile>();
            
            weaponObjectInHand.transform.parent = heldWeaponSpot;
        }
    }

    private void SolveAttacking()
    {
        if (!input.throwDown || isAttacking || IsBlockedFromAttacking || WeaponIsEmptyHand) return;
        
        if (attackCoroutine != null) StopCoroutine(attackCoroutine);
        attackCoroutine = StartCoroutine(Attack());
    }
    
    private IEnumerator Attack()
    {
        isAttacking = true;
        
        playerBehavior.AnimatorTriggerThrowAnimation();
        
        float timeout = 1f;
        while (!playerBehavior.AnimatorThrowPoint || timeout <= 0f)
        {
            timeout -= Time.deltaTime;
            yield return waitForEndOfFrame;
        }

        isAttacking = false;
        
        if (weaponObjectInHand != null)
        {
            var forward = IsAiming ?
                CameraManager.Instance.ActiveVcam.transform.forward :
                playerBehavior.PlayerMovement.ForwardDirection;
            
            weaponObjectInHand.Throw(selectedWeapon.throwForce, forward);
            weaponObjectInHand = null;
        }

        UpdateWeaponObjectOnHand();
    }
}
