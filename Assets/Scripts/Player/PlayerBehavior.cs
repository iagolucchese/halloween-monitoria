﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class PlayerBehavior : MonoBehaviour
{
    private static PlayerBehavior _instance;
    public static PlayerBehavior Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogWarning("PlayerBehavior didnt exist on GET");
            }

            return _instance;
        }
    }

    [Header("Events")]
    public UnityAction<int> OnWeaponChanged;
    public UnityAction OnSprintStart;
    public UnityAction OnSprintEnd;
    public UnityAction OnAimingStart;
    public UnityAction OnAimingEnd;
    //public UnityAction OnPlayerJumped;
    public UnityAction<int> OnPlayerOutfitChanged;
    
    #pragma warning disable 0649
    [Header("Player Aspects")] 
    private PlayerMovementController playerMovement;
    private PlayerGrabThrowBehavior playerGrabThrow;
    private PlayerAttack playerAttack;
    private PlayerAnimationFunctions playerAnimationFunctions;
    
    public PlayerMovementController PlayerMovement => playerMovement != null ? 
        playerMovement : 
        playerMovement = GetComponent<PlayerMovementController>();
    public PlayerGrabThrowBehavior PlayerGrabThrow => playerGrabThrow != null ? 
        playerGrabThrow : 
        playerGrabThrow = GetComponentInChildren<PlayerGrabThrowBehavior>();
    public PlayerAttack PlayerAttack => playerAttack != null ? 
        playerAttack : 
        playerAttack = GetComponent<PlayerAttack>();
    
    public PlayerAnimationFunctions PlayerAnimationFunctions => playerAnimationFunctions != null ? 
        playerAnimationFunctions : 
        playerAnimationFunctions = GetComponentInChildren<PlayerAnimationFunctions>();
    
    [Header("Animation")]
    [HideInInspector] public List<Animator> allAnimators;
    [HideInInspector] public Animator movementAnimator;
    private bool animatorGrabPoint;
    private bool animatorThrowPoint;
    
    public float AnimatorMoveSpeed
    {
        get => movementAnimator.GetFloat(MoveSpeed);
        set => movementAnimator.SetFloat(MoveSpeed, value);
    }
    public bool AnimatorThrowPoint
    {
        get
        {
            var v = animatorThrowPoint;
            animatorThrowPoint = false;
            return v;
        }
        set => animatorThrowPoint = value;
    }
    public bool AnimatorGrabPoint
    {
        get
        {
            var v = animatorGrabPoint;
            animatorGrabPoint = false;
            return v;
        }
        set => animatorGrabPoint = value;
    }
    
    public void AnimatorTriggerGrabAnimation() {movementAnimator.SetTrigger(Grab);}
    public void AnimatorTriggerThrowAnimation() {movementAnimator.SetTrigger(Throw);}
    public void AnimatorBoolAimAnimation(bool value){movementAnimator.SetBool(Aim, value);}

    public static readonly int MoveSpeed = Animator.StringToHash("MoveSpeed");
    public static readonly int Throw = Animator.StringToHash("Throw");
    public static readonly int Grab = Animator.StringToHash("Grab");
    public static readonly int Aim = Animator.StringToHash("Aim");

    [Header("Weapons")] 
    public WeaponList weaponList;
    public List<Weapon> listOfWeapons;
    public Weapon WeaponFromIndex(int index) {return index >= listOfWeapons.Count ? null : listOfWeapons[index];}

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(gameObject);
        else 
            _instance = this;
    
        allAnimators = new List<Animator>(GetComponentsInChildren<Animator>());
        movementAnimator = allAnimators.First(); //assume que o primeiro animador é o correto
        
        //Hardcoded
        //var weapons = Resources.Load<WeaponList>("WeaponAssets/WeaponList");
        //var weapons = Resources.FindObjectsOfTypeAll<WeaponList>().FirstOrDefault();
        weaponList = WeaponList.GetInstance();
        listOfWeapons = weaponList.allWeapons;
        
        #if (DEBUG1)
            OnPlayerJumped += () => Debug.Log("OnPlayerJumped");
            OnSprintStart += () => Debug.Log("OnSprintStart");
            OnSprintEnd += () => Debug.Log("OnSprintEnd");
            OnWeaponChanged += x => Debug.Log("OnWeaponChanged to weapon " + x);
        #endif
    }
}