﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;

public class PlayerGrabThrowBehavior : MonoBehaviour
{
    private const int EmptyHandSlot = 0; //hardcoded
    
    public GrabbableItem itemBeingHeld;
    [SerializeField] private Transform heldItemSpot;

    [Header("Aiming")] 
    public CinemachineVirtualCameraBase aimVcam;
    public bool isAiming;
    public int aimBlock;

    [Header("Grabbing")]
    [SerializeField] private List<GrabbableItem> objectsInGrabRange;
    public bool isGrabbing;
    public int grabBlock;
    
    [Header("Throwing")]
    public Vector3 throwForce;
    public bool isThrowing;
    public int throwBlock;
    
    private bool isEmptyHandSelected;

    private bool IsBlockedFromAiming => aimBlock > 0;
    private bool IsBlockedFromGrabbing => grabBlock > 0;
    private bool IsBlockedFromThrowing => throwBlock > 0;
    private bool HasAnyObjectInGrabRange => objectsInGrabRange.Count > 0;
        
    private InputManager input;
    private PlayerBehavior playerBehavior;
    private Coroutine aimCoroutine;
    private Coroutine throwCoroutine;

    private WaitForEndOfFrame waitForEndOfFrame;
    
    private void Start()
    {
        waitForEndOfFrame = new WaitForEndOfFrame();
        
        input = InputManager.Instance;
        playerBehavior = PlayerBehavior.Instance;

        playerBehavior.OnWeaponChanged += HandleWeaponChange;
        isEmptyHandSelected = playerBehavior.PlayerAttack.SelectedWeaponNumber == EmptyHandSlot;
        
        objectsInGrabRange = objectsInGrabRange ?? new List<GrabbableItem>();
        
        if (heldItemSpot != null) return;
        heldItemSpot = transform;
        Debug.LogWarning("Player held item spot null, using " + name + "'s transform");
    }
    
    private void HandleWeaponChange(int selectedWeapon)
    {
        isEmptyHandSelected = selectedWeapon == EmptyHandSlot;

        if (!itemBeingHeld) return;
        itemBeingHeld.Drop();
        itemBeingHeld = null;
    }

    private void Update()
    {
        SolveGrabbing();
        SolveAimThrowing();
    }
    
    #region Grabbing Functions
    
    private void SolveGrabbing()
    {
        if (!input.grabDown || !isEmptyHandSelected || isThrowing || isAiming) return;
        
        if (itemBeingHeld)
        {
            itemBeingHeld.Drop();
            itemBeingHeld = null;
        }
        else
        {
            if (!IsBlockedFromGrabbing && !isGrabbing && HasAnyObjectInGrabRange)
                GrabFirstItem();
        }
    }

    private void GrabFirstItem()
    {
        var item = objectsInGrabRange.FirstOrDefault(x => x.CanBeGrabbed);
        if (!item) return;

        StartCoroutine(Grab(item));
    }
    
    private IEnumerator Grab(GrabbableItem item)
    {
        isGrabbing = true;

        //assumes animation will set playerBehavior.AnimatorGrabPoint to TRUE eventually
        playerBehavior.AnimatorTriggerGrabAnimation();

        yield return new WaitUntil(() => playerBehavior.AnimatorGrabPoint);
        
        isGrabbing = false;

        if (objectsInGrabRange.Contains(item))
            objectsInGrabRange.Remove(item);
        
        item.Pickup(heldItemSpot);
        itemBeingHeld = item;
    }
    
    #endregion
    
    #region Aiming/Throwing Functions
    
    private void SolveAimThrowing()
    {
        if (input.aimDown && !IsBlockedFromAiming && !isAiming)
        {
            if (aimCoroutine != null) StopCoroutine(aimCoroutine);
            aimCoroutine = StartCoroutine(Aim());
        }
        else if (input.throwDown && !IsBlockedFromThrowing && !isThrowing && isEmptyHandSelected)
        {
            if (throwCoroutine != null) StopCoroutine(throwCoroutine);
            throwCoroutine = StartCoroutine(Throw());
        }
    }

    private IEnumerator Aim()
    {
        //start aiming
        isAiming = true;
        playerBehavior.AnimatorBoolAimAnimation(true);
        CameraManager.Instance.ActiveVcam = aimVcam;
        playerBehavior.OnAimingStart?.Invoke();
        
        //hold aiming
        while (input.aim || isThrowing || playerBehavior.PlayerAttack.isAttacking)
        {
            /*if (input.throwDown && !isThrowing && !IsBlockedFromThrowing)
            {
                if (throwCoroutine != null) StopCoroutine(throwCoroutine);
                throwCoroutine = StartCoroutine(Throw());
                //yield return new WaitUntil( () => isThrowing = false);
            }*/
            yield return waitForEndOfFrame;
        }
        
        //end aiming
        isAiming = false;
        if (throwCoroutine != null) StopCoroutine(throwCoroutine); //ends a throw that was happening when we stopped aiming
        playerBehavior.AnimatorBoolAimAnimation(false);
        CameraManager.Instance.ResetCameraToDefault();
        playerBehavior.OnAimingEnd?.Invoke();
    }

    private IEnumerator Throw()
    {
        isThrowing = true;

        //assumes animation will set playerBehavior.AnimatorThrowPoint to TRUE eventually
        playerBehavior.AnimatorTriggerThrowAnimation();
        
        //yield return new WaitUntil(() => playerBehavior.AnimatorThrowPoint );
        float timeout = 1f;
        while (!playerBehavior.AnimatorThrowPoint || timeout <= 0f)
        {
            timeout -= Time.deltaTime;
            yield return waitForEndOfFrame;
        }

        isThrowing = false;

        if (itemBeingHeld != null)
        {
            var forward = isAiming ?
                CameraManager.Instance.ActiveVcam.transform.forward :
                playerBehavior.PlayerMovement.ForwardDirection;
            
            itemBeingHeld.Throw(throwForce, forward);
            itemBeingHeld = null;
        }
    }
  
    #endregion

    private void AddToObjetsInRange(GrabbableItem grab)
    {
        grab.ToggleOutline(true);
        objectsInGrabRange.Add(grab);
    }
    
    private void RemoveFromObjetsInRange(GrabbableItem grab)
    {
        grab.ToggleOutline(false);
        objectsInGrabRange.Remove(grab);
    }

    private void OnTriggerEnter(Collider other)
    {
        var grab = other.gameObject.GetComponent<GrabbableItem>();
        if (grab != null)
            AddToObjetsInRange(grab);
    }

    private void OnTriggerStay(Collider other)
    {
        //TODO: remover por performance?
        var grab = other.gameObject.GetComponent<GrabbableItem>();
        if (grab != null && !objectsInGrabRange.Contains(grab))
            AddToObjetsInRange(grab);
    }

    private void OnTriggerExit(Collider other)
    {
        var grab = other.gameObject.GetComponent<GrabbableItem>();
        if (grab != null && objectsInGrabRange.Contains(grab))
            RemoveFromObjetsInRange(grab);
    }
}
