﻿using UnityEngine;

[RequireComponent(typeof(PlayerBehavior))]
public class PlayerMovementController : MonoBehaviour
{
    public PlayerMovementParams movementParams;
    
    [Header("Movement")]
    [SerializeField] private Vector3 movementDirection = Vector3.zero;
    [SerializeField] private float currentSpeed;
    public bool isGrounded;
    public int freezeMovement;

    [Header("Aiming")] 
    public Vector2 aimVerticalRotationClamp = new Vector2(5f, 89f);
    public float aimVerticalSensitivity = 1f;
    public float aimHorizontalSensitivity = 1f;
    public bool invertVertical = true;

    [Header("Sprinting")]
    public bool isAllowedToSprint = true;
    public bool canSprint = true;
    public bool isSprinting;
    [Range(0f, 1f)] public float stamina = 1f;
    
    //properties
    private bool HasZeroMovementInput => Mathf.Abs(input.verticalInput) <= ZeroMovementThreshold && Mathf.Abs(input.horizontalInput) <= ZeroMovementThreshold;
    public Vector3 ForwardDirection => myTransform.forward;

    private Transform myTransform;
    private Transform thirdPersonCameraTransform;
    private Quaternion playerRotation;
    private PlayerBehavior playerBehavior;
    private InputManager input;
    private CharacterController controller;
    private Animator playerMovementAnimator;
    private ParticleSystem footstepParticles;

    private Vector3 targetRotation;
    
    private static readonly float ZeroMovementThreshold = Mathf.Epsilon;

    private void Start()
    {
        input = InputManager.Instance;
        playerBehavior = PlayerBehavior.Instance;
        playerMovementAnimator = playerBehavior.movementAnimator;
        myTransform = transform;

        controller = GetComponent<CharacterController>();
        footstepParticles = GetComponentInChildren<ParticleSystem>();
        
        movementParams = movementParams ? movementParams : Resources.Load<PlayerMovementParams>("Player/" + PlayerMovementParams.DefaultFilename);

        thirdPersonCameraTransform = FindObjectOfType<ThirdPersonCameraController>().transform;
        
        //events
        playerBehavior.OnAimingStart += SetUpAimingMode;
        playerBehavior.OnAimingEnd += DisableAimingMode;
    }

    private void SolveSprinting()
    {
        if (isAllowedToSprint)
        {
            if (stamina <= 0)
                canSprint = false;
            else if (stamina >= 1f)
                canSprint = true;

            if (canSprint && input.sprint)
            {
                if (!isSprinting) playerBehavior.OnSprintStart?.Invoke();
                isSprinting = true;
            }
            else
            {
                if (isSprinting) playerBehavior.OnSprintEnd?.Invoke();
                isSprinting = false;
            }
        }

        if (isSprinting && !HasZeroMovementInput)
            stamina -= Time.deltaTime * movementParams.staminaDrainFromSprint;
        else
            stamina += Time.deltaTime * (canSprint ? movementParams.staminaRecoverSpeed : movementParams.staminaRecoverWhenExhausted);
        
        stamina = Mathf.Clamp01(stamina);
    }

    private void SolvePlayerMovement()
    {
        //determina a direcao do player
        var step = thirdPersonCameraTransform.forward * input.verticalInput;
        step += thirdPersonCameraTransform.right * input.horizontalInput;
        step.y = 0f;
        step.Normalize();
        movementDirection = new Vector3 (step.x, movementDirection.y, step.z);
        
        if (freezeMovement > 0)
            movementDirection.x = movementDirection.z = 0f;
        
        //rotaciona o player no X e no Z, se necessario
        var desiredForward = movementDirection;
        desiredForward.y = 0f;
        if (desiredForward.sqrMagnitude > ZeroMovementThreshold)
        {
            var finalForward = Vector3.Lerp(transform.forward, desiredForward, Time.fixedDeltaTime * movementParams.rotationSpeed);
            transform.forward = finalForward;
        }

        //aceleracao
        if (HasZeroMovementInput)
            currentSpeed = currentSpeed <= 0f ? 0f : currentSpeed - movementParams.deceleration;
        else if (currentSpeed < (isSprinting ? movementParams.sprintMoveSpeed : movementParams.walkingMoveSpeed))
            currentSpeed += isSprinting ? movementParams.sprintAcceleration : movementParams.acceleration;
        
        //gravidade
        if (isGrounded && movementDirection.y <= 0f)
            movementDirection.y = 0f;
        else
            movementDirection.y += movementParams.playerGravity * Time.fixedDeltaTime;

        SolveJumping();

        //atualiza as variaveis dos animators
        playerBehavior.AnimatorMoveSpeed = currentSpeed;
        //dustParticleAnimator?.SetFloat(MoveSpeed, isGrounded ? currentSpeed : 0f);
        
        //mover de fato
        var movementTimesSpeed = Vector3.Scale(movementDirection, new Vector3(currentSpeed, 1f, currentSpeed));
        controller.Move(Time.fixedDeltaTime * movementTimesSpeed); //move speed afeta o pulo ainda
    }

    private void SolveAimingMovement()
    {
        var verticalRotation = input.cameraVerticalInput * Time.deltaTime * aimVerticalSensitivity;
        var horizontalRotation = input.cameraHorizontalInput * Time.deltaTime * aimHorizontalSensitivity;
        targetRotation.y += horizontalRotation;
        targetRotation.x += verticalRotation * (invertVertical ? -1f : 1f);
        targetRotation.x = Mathf.Clamp(targetRotation.x, aimVerticalRotationClamp.x, aimVerticalRotationClamp.y);

        transform.rotation = Quaternion.Euler(targetRotation);
    }

    private void SetUpAimingMode()
    {
        //freezeMovement++;
        targetRotation = transform.eulerAngles;
    }

    private void DisableAimingMode()
    {
        //freezeMovement--;
        targetRotation.x = 0f;
        transform.eulerAngles = targetRotation;
    }

    private void SolveJumping()
    {
        /*if (!input.jumpDown || !isGrounded) return;
        
        isGrounded = false;
        var speedFactor = movementParams.speedFactorToJumpHeight * currentSpeed;
        movementDirection.y = Mathf.Sqrt(Time.fixedDeltaTime * movementParams.jumpHeight * 
                                         -2f * movementParams.playerGravity * 
                                         (speedFactor > 1f ? speedFactor : 1f) 
        );
        playerBehavior.OnPlayerJumped?.Invoke();*/
    }

    private void Update()
    {
        if (Time.timeScale <= 0) return;
        SolveSprinting();
    }

    private void FixedUpdate()
    {
        if (Time.timeScale <= 0) return;
        if (playerBehavior.PlayerGrabThrow.isAiming)
            SolveAimingMovement();
        else
            SolvePlayerMovement();
    }
}
