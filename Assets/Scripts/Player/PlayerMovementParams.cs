﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = DefaultFilename, menuName = "Player/Movement Params")]
public class PlayerMovementParams : ScriptableObject
{
    public const string DefaultFilename = "PlayerMovementParams";
    
    [Header("Parametros de Movimento")]
    public float playerGravity = Physics.gravity.y;
    //public float baseMoveSpeed = 4f;
    public float walkingMoveSpeed = 8f;
    public float acceleration = 0.1f;
    public float deceleration = 0.25f;
    public float jumpHeight = 10f;
    public float speedFactorToJumpHeight;
    //public float terminalSpeed = 50f;
    public float rotationSpeed = 15f;

    [Header("Stamina e Corrida")]
    public float sprintMoveSpeed = 15f;
    public float sprintAcceleration = 0.2f;
    public float staminaDrainFromSprint = 0.2f;
    public float staminaRecoverSpeed = 0.5f;
    public float staminaRecoverWhenExhausted = 0.25f;
}
