﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerTextureSwap : MonoBehaviour
{
    #pragma warning disable 0649
    [SerializeField] private int startingOutfit;
    [SerializeField] private Renderer playerMesh;
    [SerializeField] private List<Material> outfitMaterialList;
    [SerializeField] private int selectedOutfit;
    private int SelectedOutfit
    {
        get => selectedOutfit;
        set
        {
            selectedOutfit = value < 0 ? outfitMaterialList.Count-1 : value >= outfitMaterialList.Count ? 0 : value;
            ChangeToOutfit(selectedOutfit);
        }
    }
    
    private PlayerBehavior playerBehavior;
    private void Start()
    {
        playerBehavior = PlayerBehavior.Instance;
        
        if (playerMesh == null)
            Debug.LogError("PlayerMesh in PlayerTextureSwap is null.");
        SelectedOutfit = startingOutfit;
    }

    public void ChangeToOutfit(int outfit)
    {
        playerMesh.material = outfitMaterialList[outfit];
        playerBehavior.OnPlayerOutfitChanged?.Invoke(outfit);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
            SelectedOutfit--;
        if (Input.GetKeyDown(KeyCode.L))
            SelectedOutfit++;
    }
}
