﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerGrabThrowBehavior : MonoBehaviour
{
    public GrabbableItem itemBeingHeld;
    [SerializeField] private Transform heldItemSpot;
    
    private List<GrabbableItem> objectsInRange;
    [HideInInspector] public bool isGrabbing;
    [HideInInspector] public bool isThrowing;
    [HideInInspector] public int grabBlock;
    [HideInInspector] public int throwBlock;
    
    [Header("Throw Params")]
    public Vector3 throwForce;
    private bool isEmptyHandSelected;
    
    private bool IsBlockedFromGrabbing => grabBlock > 0;
    private bool IsBlockedFromThrowing => throwBlock > 0;
    private bool HasAnyObjectInGrabRange => objectsInRange.Count > 0;
        
    private InputManager input;
    private PlayerBehavior playerBehavior;
    
    private void Start()
    {
        input = InputManager.Instance;
        playerBehavior = PlayerBehavior.Instance;

        playerBehavior.OnWeaponChanged += HandleWeaponChange;
        isEmptyHandSelected = playerBehavior.PlayerAttack.SelectedWeaponNumber == 0;
        
        objectsInRange = objectsInRange ?? new List<GrabbableItem>();
        
        if (heldItemSpot != null) return;
        heldItemSpot = transform;
        Debug.LogWarning("Player held item spot null, using " + name + "'s transform");
    }

    private void Update()
    {
        SolveGrabbing();
        SolveThrowing();
    }

    private void HandleWeaponChange(int selectedWeapon)
    {
        //TODO: hardcoded
        isEmptyHandSelected = selectedWeapon == 0;

        if (!itemBeingHeld) return;
        itemBeingHeld.Drop();
        itemBeingHeld = null;
    }
    
    private void SolveGrabbing()
    {
        if (!input.grabDown || !isEmptyHandSelected || isThrowing) return;
        
        if (itemBeingHeld)
        {
            itemBeingHeld.Drop();
            itemBeingHeld = null;
        }
        else
        {
            if (!IsBlockedFromGrabbing && !isGrabbing && HasAnyObjectInGrabRange)
                GrabFirstItem();
        }
    }

    private void SolveThrowing()
    {
        if (!input.throwDown || !itemBeingHeld || IsBlockedFromThrowing || isThrowing) return;
        StartCoroutine(Throw());
    }

    private IEnumerator Throw()
    {
        isThrowing = true;

        //assumes animation will set playerBehavior.AnimatorThrowPoint to TRUE eventually
        playerBehavior.AnimatorTriggerThrowAnimation();
        
        yield return new WaitUntil(() => playerBehavior.AnimatorThrowPoint);

        isThrowing = false;
        if (itemBeingHeld != null)
        {
            itemBeingHeld.Throw(throwForce, playerBehavior.PlayerMovement.ForwardDirection);
            itemBeingHeld = null;
        }
    }

    private void GrabFirstItem()
    {
        var item = objectsInRange.FirstOrDefault(x => x.CanBeGrabbed);
        if (!item) return;

        StartCoroutine(Grab(item));
    }

    private IEnumerator Grab(GrabbableItem item)
    {
        isGrabbing = true;

        //assumes animation will set playerBehavior.AnimatorGrabPoint to TRUE eventually
        playerBehavior.AnimatorTriggerGrabAnimation();

        yield return new WaitUntil(() => playerBehavior.AnimatorGrabPoint);
        
        isGrabbing = false;

        if (objectsInRange.Contains(item))
            objectsInRange.Remove(item);
        
        item.Pickup(heldItemSpot);
        itemBeingHeld = item;
    }

    private void AddToObjetsInRange(GrabbableItem grab)
    {
        grab.ToggleOutline(true);
        objectsInRange.Add(grab);
    }
    
    private void RemoveFromObjetsInRange(GrabbableItem grab)
    {
        grab.ToggleOutline(false);
        objectsInRange.Remove(grab);
    }

    private void OnTriggerEnter(Collider other)
    {
        var grab = other.gameObject.GetComponent<GrabbableItem>();
        if (grab != null)
            AddToObjetsInRange(grab);
    }

    private void OnTriggerStay(Collider other)
    {
        //TODO: remover por performance?
        var grab = other.gameObject.GetComponent<GrabbableItem>();
        if (grab != null && !objectsInRange.Contains(grab))
            AddToObjetsInRange(grab);
    }

    private void OnTriggerExit(Collider other)
    {
        var grab = other.gameObject.GetComponent<GrabbableItem>();
        if (grab != null && objectsInRange.Contains(grab))
            RemoveFromObjetsInRange(grab);
    }
}
