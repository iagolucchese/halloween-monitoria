﻿using UnityEngine;

public class PortaBehavior : MonoBehaviour
{
    public float speed = 1f;
    public Transform PontoZero;
    public Transform PontoUm;
    public Transform PontoDois;
    public Transform portaDeFato;

    public bool indoParaFora;
    public bool moverPorta;
    
    public Transform target;

    private void Start()
    {
        target = PontoZero;
    }

    private void Update()
    {
        if (moverPorta)
            MoverPorta();
    }

    private void MoverPorta()
    {
        //decide para onde ele vai
        
        if (indoParaFora) //se indo pra fora
        {
            if (Vector3.Distance(portaDeFato.position, PontoUm.position) <= 0)
            {
                target = PontoDois;
            }
            else if (Vector3.Distance(portaDeFato.position, PontoDois.position) <= 0)
            {
                moverPorta = false;
            }
        }
        else //se esta indo pra dentro
        {
            if (Vector3.Distance(portaDeFato.position, PontoUm.position) <= 0)
            {
                target = PontoZero;
            }
            else if (Vector3.Distance(portaDeFato.position, PontoZero.position) <= 0f)
            {
                moverPorta = false;
            }
        }
        
        portaDeFato.position = Vector3.MoveTowards(portaDeFato.position, target.position, speed * Time.deltaTime);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            moverPorta = true;
            indoParaFora = true;
            target = PontoUm;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            moverPorta = true;
            indoParaFora = false;
            target = PontoUm;
        }
    }
}
