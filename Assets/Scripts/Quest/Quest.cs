﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Quest", menuName = GameLauncher.GameName + "/Quest")]
[System.Serializable]
public class Quest : ScriptableObject
{
    public string questName = "New Quest";
    public string questDescription = "New Quest Description";
    public List<QuestObjective> objectives;
    public List<QuestReward> rewards;
    public bool isActiveOnStart = true;
    public bool completedWithSuccess;
    public int numberOfCompletedObjectives;
    public bool isQuestActive;

    private void QuestObjectiveCompleted(QuestObjective completedObjective)
    {
        numberOfCompletedObjectives++;
        completedObjective.onObjectiveCompleted -= QuestObjectiveCompleted;
        if (numberOfCompletedObjectives >= objectives.Count)
        {
            SetQuestEnded();
        }
    }

    public void AddNewObjectiveToQuest(QuestObjective objective)
    {
        if (completedWithSuccess || objective == null) return;
        objectives.Add(objective);
        objective.onObjectiveCompleted += QuestObjectiveCompleted;
    }

    public void SetQuestActive()
    {
        QuestManager.Instance.AddQuestToActive(this);

        foreach (QuestObjective objective in objectives)
        {
            objective.onObjectiveCompleted += QuestObjectiveCompleted;
            Debug.Log(questName + " is watching " + objective.objectiveName);
        }
        isQuestActive = true;
    }

    public void SetQuestEnded(bool withSuccess = true)
    {
        isQuestActive = false;
        QuestManager.Instance.RemoveQuestFromActive(this, true);
        
        if (withSuccess)
        {
            Debug.Log(questName + " is completed with success!");
            completedWithSuccess = true;
            foreach (var reward in rewards)
            {
                reward.GrantReward();
            }
        }
        else
        {
            Debug.Log(questName + " is completed!");
        }
    }

    public void ResetAllObjectives()
    {
        foreach (var item in objectives)
        {
            item.ResetObjective();
        }
        numberOfCompletedObjectives = 0;
        completedWithSuccess = false;
    }
}
