﻿using System.Collections.Generic;
using UnityEngine;

public class QuestManager : ASingleton<QuestManager>
{
    public const string QuestsResourcesFolder = "Quests";
    
    public Quest trackedQuest;

    /// <summary>
    /// Lista de todas as quests, a principio nao se deve adicionar ou remover dela durante execucao.
    /// </summary>
    [SerializeField] private List<Quest> allQuestsList;
    [SerializeField] private List<Quest> completedQuestList;
    [SerializeField] private List<Quest> activeQuestList;

    public List<Quest> AllQuestsList{
        get => allQuestsList;
        private set => allQuestsList = value;
    }
    public List<Quest> ActiveQuestList{
        get => activeQuestList;
        private set => activeQuestList = value;
    }
    public List<Quest> CompletedQuestList{
        get => completedQuestList;
        private set => completedQuestList = value;
    }
    
    private void Start()
    {
        LoadQuestsFromFolder();
    }

    /// <summary>
    /// Carrega as quests registradas na pasta Resources + QuestsResourcesFolder
    /// </summary>
    public void LoadQuestsFromFolder()
    {
        allQuestsList = new List<Quest>();

        var list = Resources.LoadAll<Quest>(QuestsResourcesFolder);
        foreach (var quest in list)
        {
            allQuestsList.Add(quest);
            if (quest.isActiveOnStart)
            {
                quest.SetQuestActive();
            }
        }
    }

    public bool AddQuestToActive(Quest quest)
    {
        if (ActiveQuestList.Contains(quest)) return false;

        ActiveQuestList.Add(quest);
        trackedQuest = quest;
        return true;
    }

    public bool RemoveQuestFromActive(Quest quest, bool hasCompleted = true)
    {
        if (!ActiveQuestList.Contains(quest)) return false;
        
        RemoveQuest(quest, hasCompleted);
        return true;
    }

    public bool RemoveQuestFromActive(int questIndex, bool hasCompleted = true)
    {
        if (ActiveQuestList[questIndex] == null) return false;
        
        RemoveQuest(ActiveQuestList[questIndex], hasCompleted);
        return true;
    }

    private void RemoveQuest(Quest quest, bool hasCompleted)
    {
        if (hasCompleted)
            CompletedQuestList.Add(quest);
            
        if (quest == trackedQuest)
        {
            trackedQuest = ActiveQuestList.Count > 0 ? ActiveQuestList[ActiveQuestList.Count-1] : null;
        }
        ActiveQuestList.Remove(quest);
    }
}
