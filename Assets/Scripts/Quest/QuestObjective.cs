﻿using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "NewQuestObjective", menuName = GameLauncher.GameName + "/Quest Objective")]
[System.Serializable]
public class QuestObjective : ScriptableObject
{
    public enum ObjectiveTypes
    {
        Boolean, SimpleCounter
    }
    public string objectiveName = "New Objective";
    public string objectiveDescription = "Objective Description Here.";
    public bool canLoseProgressAfterCompletion;
    
    public ObjectiveTypes objectiveType;
    public UnityAction<QuestObjective> onObjectiveCompleted;

    //se for um objetivo contador, usa isso
    public int CounterGoal = 1;
    [SerializeField] private int counterCurrentProgress;
    public int CounterCurrentProgress
    {
        get => counterCurrentProgress;
        set => counterCurrentProgress = value < 0 ? 0 : value > CounterGoal ? CounterGoal : value;
    }

    //se for um objetivo booleano, vai usar isso
    public bool ObjectiveHasCompleted;

    private void HandleObjectiveCompleted()
    {
        if (ObjectiveHasCompleted) return;
        
        Debug.Log(objectiveName + " completed");

        //se nao tem nenhuma quest cuidando desse objetivo, ignora
        if (onObjectiveCompleted == null) return;

        ObjectiveHasCompleted = true;
        onObjectiveCompleted.Invoke(this);
    }

    public void AddProgressToObjective()
    {
        if (objectiveType == ObjectiveTypes.Boolean)
        {
            HandleObjectiveCompleted();
        }
    }

    public void AddProgressToObjective(int amountOfProgress)
    {
        if (objectiveType != ObjectiveTypes.SimpleCounter)
        {
            Debug.LogError(name + "'s trigger isn't a SimpleCounter, but AddProgressToObjective(int) was called.");
            return;
        }
        
        if (!canLoseProgressAfterCompletion && ObjectiveHasCompleted) return;
        
        Debug.Log(objectiveName + " added progress: " + amountOfProgress);

        CounterCurrentProgress += amountOfProgress;
        if (CounterCurrentProgress >= CounterGoal)
        {
            HandleObjectiveCompleted();
        }
        else if (ObjectiveHasCompleted)
        {
            ObjectiveHasCompleted = false;
        }
    }

    public void ResetObjective()
    {
        ObjectiveHasCompleted = false;
        CounterCurrentProgress = 0;
    }
}