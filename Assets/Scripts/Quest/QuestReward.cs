﻿using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "NewQuestReward", menuName = GameLauncher.GameName + "/Quest Reward")]
[System.Serializable]
public class QuestReward : ScriptableObject
{
    public string rewardName = "New Reward";
    public UnityEvent reward;

    public void GrantReward()
    {
        reward?.Invoke();
    }

    private void OnEnable()
    {
        reward.AddListener(DebugReward);
    }

    public void DebugReward()
    {
        Debug.Log(name + " QuestReward has been granted.");
    }
}
