﻿using UnityEngine;
using UnityEngine.Events;

public class QuestTrigger : MonoBehaviour
{
    public enum TriggerTypes
    {
        Collision, Time, Quest, Boolean
    }

    public string triggerName = "NewTrigger";
    public bool isActive = true;
    public bool firesMoreThanOnce;
    public bool hasAlreadyFired;
    public bool canBeUntriggered;
    public TriggerTypes triggerType = TriggerTypes.Collision;
    public UnityEvent onTriggerActivatedEvent;
    public UnityEvent onTriggerRevertedEvent;

    //position: funciona via um evento de trigger
    private new Collider collider;
    public string checkForTag = "";

    //time: espera X segundos para ativar
    public bool doCountdown;
    public float timeToWait = 1f;

    //quest: uma quest anterior ativou esse trigger
    public Quest questToTrigger;

    //boolean: algum evento ou ação tem que ativar o trigger    
    public bool shouldTrigger;
    public UnityEvent shouldTriggerEvent;

    private void Start() 
    {
        collider = GetComponent<Collider>();
        if (triggerType == TriggerTypes.Collision && collider == null)
            Debug.LogError(name + "has a Collision TriggerType, but no collider.");
    }
    
    public void MakeTriggerActive()
    {
        isActive = true;
    }

    private void FireTrigger(bool forceTrigger = false)    
    {
        if (hasAlreadyFired && !firesMoreThanOnce && !forceTrigger) return;
        
        onTriggerActivatedEvent?.Invoke();
        hasAlreadyFired = true;
        
        Debug.Log(name + " trigger fired.");
        //TODO
    }

    private void RevertTrigger()
    {
        if (!canBeUntriggered) return;
        
        onTriggerRevertedEvent?.Invoke();
        Debug.Log(name + " trigger reverted.");
    }

    public void TimeTriggerCountdown(bool start)
    {
        if (triggerType == TriggerTypes.Time)
            doCountdown = start;
    }

    public void BooleanTriggerFire()
    {
        if (triggerType == TriggerTypes.Boolean)
            shouldTrigger = true;
    }

    private void Update()
    {
        if (!isActive) return;

        switch (triggerType)
        {
            case TriggerTypes.Collision:
                break;
            case TriggerTypes.Time:
                if (doCountdown)
                {
                    if (timeToWait > 0f)
                        timeToWait -= Time.deltaTime;
                    else
                        FireTrigger();
                }
                break;
            case TriggerTypes.Quest:
                if (questToTrigger == null)
                    Debug.LogError(triggerName + "is Quest-type but QuestToTrigger is null.");
                else
                {
                    if (questToTrigger.completedWithSuccess)
                        FireTrigger();
                }
                break;
            case TriggerTypes.Boolean:
                if (shouldTrigger)
                    FireTrigger();
                break;
            default:
                Debug.LogError(triggerName + "has no TriggerType(?)");
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (triggerType != TriggerTypes.Collision) return;
        
        //se o CheckForTag tiver algo escrito nele, testa pra ver se o objeto colidido tem essa tag, senao colide com qualquer coisa
        if (string.IsNullOrEmpty(checkForTag) || other.gameObject.CompareTag(checkForTag))
            FireTrigger();
    }

    private void OnTriggerExit(Collider other)
    {
        if (triggerType != TriggerTypes.Collision) return;
        
        //se o CheckForTag tiver algo escrito nele, testa pra ver se o objeto colidido tem essa tag, senao colide com qualquer coisa
        if (canBeUntriggered && (string.IsNullOrEmpty(checkForTag) || other.gameObject.CompareTag(checkForTag)))
            RevertTrigger();
    }
}
