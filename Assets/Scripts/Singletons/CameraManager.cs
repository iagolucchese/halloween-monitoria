﻿using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class CameraManager : ASingleton<CameraManager>
{
    public CinemachineVirtualCameraBase defaultVcam;
    [SerializeField] private CinemachineVirtualCameraBase activeVcam;
    public CinemachineVirtualCameraBase ActiveVcam
    {
        get
        {
            if (activeVcam != null) return activeVcam;
            activeVcam = listOfCameras.FirstOrDefault(x => x.enabled);
            
            if (activeVcam != null) return activeVcam;
            activeVcam = FindObjectsOfType<CinemachineVirtualCameraBase>().FirstOrDefault(x => x.enabled);
            
            return activeVcam;
        }
        set
        {
            activeVcam.enabled = false;
            value.enabled = true;
            activeVcam = value;
            onActiveCameraChanged?.Invoke(activeVcam);
        }
    }

    public List<CinemachineVirtualCameraBase> listOfCameras;
    
    public UnityAction<CinemachineVirtualCameraBase> onActiveCameraChanged;
    
    protected override void Awake()
    {
        base.Awake();
        /*if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }*/
        SceneManager.sceneLoaded += ResetOnSceneLoaded;
        if (defaultVcam == null)
        {
            defaultVcam = ActiveVcam;
        }
    }

    private void ResetOnSceneLoaded(Scene currentScene, LoadSceneMode loadSceneMode)
    {
        ResetCameraManager();
    }

    public void ResetCameraToDefault()
    {
        if (defaultVcam == null)
        {
            Debug.LogError("No default camera set on CameraManager");
            return;
        }
        ActiveVcam = defaultVcam;
    }

    private void ResetCameraManager()
    {
        activeVcam = defaultVcam;
        listOfCameras = new List<CinemachineVirtualCameraBase>();
        
        foreach (var cam in Resources.FindObjectsOfTypeAll<CinemachineVirtualCameraBase>())
        {
            if (cam.hideFlags == HideFlags.NotEditable || cam.hideFlags == HideFlags.HideAndDontSave)
                continue;

            if (EditorUtility.IsPersistent(cam.transform.root.gameObject))
                continue;

            listOfCameras.Add(cam);
            //leaves only the camera set as default enabled, disables the rest
            //cam.enabled = cam.Equals(activeVcam);
        }
    }
}