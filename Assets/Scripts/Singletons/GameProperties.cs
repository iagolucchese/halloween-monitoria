﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = DefaultName, menuName = GameLauncher.GameName+"/Game Properties")]
public class GameProperties : ScriptableObject
{
    private const string DefaultName = "GDD_GameProperties";
    
    private static GameProperties _instance;
    public static GameProperties Instance{
        get{
            if(_instance != null)
                return _instance;

            _instance = (GameProperties)Resources.Load(DefaultName);
           
            if(_instance == null)
                Debug.LogWarning("GameProperties file not found");

            return _instance;
        }
    }

    public InputOptions inputOptions;
    public AudioOptions audioOptions;
    
    private void Awake()
    {
        inputOptions = inputOptions ?? new InputOptions();
        audioOptions = audioOptions ?? new AudioOptions();
    }
}

[System.Serializable]
public class InputOptions
{
    public Vector2 mouseCameraSensitivity = Vector2.one;
    public Vector2 controllerCameraSensitivity = Vector2.one;
}

[System.Serializable]
public class AudioOptions
{
    public float masterSoundVolume = 1f;
    public float musicSoundVolume = 1f;
    public float sfxSoundVolume = 1f;
}
