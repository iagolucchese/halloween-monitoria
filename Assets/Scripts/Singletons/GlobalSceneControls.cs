﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GlobalSceneControls : ASingleton<GlobalSceneControls>
{
    public bool isGamePaused;
    //public bool isInGameScene;
    
    private InputManager inputManager;

    public UnityAction OnGamePaused;
    public UnityAction OnGameUnpaused;
    
    private void Start()
    {
        inputManager = InputManager.Instance;
    }

    private void Update()
    {
        //if (!isInGameScene) return;
        
        //reload scene debug button
        if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.F8))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        
        //handle pausing
        if (inputManager.pauseDown)
            HandlePausing(!isGamePaused);
    }

    public void HandlePausing(bool doPause)
    {
        if (doPause)
            OnGamePaused?.Invoke();
        else            
            OnGameUnpaused?.Invoke();
        
        Time.timeScale = doPause ? 0f : 1f;
        isGamePaused = doPause;
    }

    public static void GoToScene(int sceneNumber)
    {
        SceneManager.LoadScene(sceneNumber);
    }

    public static void ExitGame()
    {
        Application.Quit();
    }

    public void DebugEvent()
    {
        Debug.Log("debug event fired");
    }
}
