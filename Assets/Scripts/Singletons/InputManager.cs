﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : ASingleton<InputManager>
{
    public bool UsingKeyboard { get => usingKeyboard; private set => usingKeyboard = value; }
    [SerializeField] private bool usingKeyboard;

    [Header("Player Movement")]
    public float horizontalInput;
    public float verticalInput;
    public bool horizontal;
    public bool vertical;

    [Header("Player Actions")]
    public bool useDown;
    public bool use;
    public bool sprintDown;
    public bool sprint;
    /*public bool jumpDown;
    public bool jump;*/
    public bool grabDown;
    public bool grab;
    public bool aimDown;
    public bool aim;
    public bool throwDown;
    public bool Throw;

    [Header("Player Secondary Actions")] 
    public bool cycleLeftDown;
    public bool cycleLeft;
    public bool cycleRightDown;
    public bool cycleRight;
    
    [Header("Camera")]
    public float cameraHorizontalInput;
    public float cameraVerticalInput;
    public int cameraZoomInput;
    public bool cameraCenterButtonDown;
    public bool cameraCenterButton;

    [Header("UI")]
    public bool submitDown;
    public bool submit;
    public bool cancelDown;
    public bool cancel;
    public bool pauseDown;
    public bool pause;

    private Dictionary<string, bool> lastInputAxisState = new Dictionary<string, bool>();
    
    private const float InputZeroComparisonThreshold = 0.0001f;
    private static bool IsAxisCloseEnoughToZero(string axisName) => (Math.Abs(Input.GetAxis(axisName))) <= InputZeroComparisonThreshold;
    private static int BoolToInt(bool value) => value ? 1 : 0;
    
    private void GrabPlayerInputs()
    {
        //assumindo que o player usa KB+M e/ou controle de XBOX
        //eixos de movimento do player
        horizontalInput = GetAxisInput("HorizontalKB","HorizontalCon", out horizontal);
        verticalInput  = GetAxisInput("VerticalKB","VerticalCon", out vertical);

        //botões de ação
        GetButtonHoldAndDownInputs("UseKB", "UseCon", out useDown, out use);
        GetButtonHoldAndDownInputs("SprintKB", "SprintCon", out sprintDown, out sprint);
        //GetButtonHoldAndDownInputs("JumpKB", "JumpCon", out jumpDown, out jump);
        GetButtonHoldAndDownInputs("GrabKB", "GrabCon", out grabDown, out grab);

        //botões secundários
        var cycle = GetAxisInput("CycleWeaponsKB", "CycleWeaponsCon");
        if (cycle >= 0)
            GetAxisInput("CycleWeaponsKB", "CycleWeaponsCon", out cycleRightDown, out cycleRight);
        
        if (cycle <= 0)
            GetAxisInput("CycleWeaponsKB", "CycleWeaponsCon", out cycleLeftDown, out cycleLeft);

        GetAxisInput("ThrowKB", "ThrowCon", out throwDown, out Throw);
        GetAxisInput("AimKB", "AimCon", out aimDown, out aim);
    }

    private void GrabCameraInputs()
    {
        //eixos de camera
        cameraHorizontalInput = GetAxisInput("Mouse X", "CameraHorizontalCon");
        cameraVerticalInput = GetAxisInput("Mouse Y", "CameraVerticalCon");
        
        //cameraZoomInput = Mathf.RoundToInt(GetAxisInput("CameraZoomKB","CameraZoomCon"));
        cameraZoomInput = UsingKeyboard
            ? BoolToInt(Input.GetButtonDown("CameraZoomKB"))
            : Mathf.RoundToInt(GetAxisInput("CameraZoomKB","CameraZoomCon"));
        
        //botões de camera
        GetButtonHoldAndDownInputs("CameraCenterKB", "CameraCenterCon", 
            out cameraCenterButtonDown, out cameraCenterButton);
    }

    private void GrabUiInputs()
    {
        GetButtonHoldAndDownInputs("SubmitKB", "SubmitCon", out submitDown, out submit);
        GetButtonHoldAndDownInputs("CancelKB", "CancelCon", out cancelDown, out cancel);
        GetButtonHoldAndDownInputs("PauseKB", "PauseCon", out pauseDown, out pause);
    }

    private float GetAxisInput(string kbAxisName, string conAxisName, out bool buttonDown, out bool button)
    {
        float output;
        buttonDown = false;
        if (!lastInputAxisState.ContainsKey(kbAxisName))
        {
            lastInputAxisState.Add(kbAxisName,false);
        }
        if (!lastInputAxisState.ContainsKey(conAxisName))
        {
            lastInputAxisState.Add(conAxisName,false);
        }
        
        if (!IsAxisCloseEnoughToZero(kbAxisName))
        {
            output = Input.GetAxis(kbAxisName);
            UsingKeyboard = true;
            button = true;

            if (!lastInputAxisState[kbAxisName])
                buttonDown = true;
            
            lastInputAxisState[kbAxisName] = true;
        } 
        else if (!IsAxisCloseEnoughToZero(conAxisName))
        {
            output = Input.GetAxis(conAxisName);
            UsingKeyboard = false;
            button = true;
            
            if (!lastInputAxisState[conAxisName])
                buttonDown = true;
            
            lastInputAxisState[conAxisName] = true;
        }
        else
        {
            output = 0f;
            button = false;
            buttonDown = false;
            lastInputAxisState[conAxisName] = false;
            lastInputAxisState[kbAxisName] = false;
        }

        return output;
    }
    
    private float GetAxisInput(string kbAxisName, string conAxisName, out bool button)
    {
        float output;
        if (!IsAxisCloseEnoughToZero(kbAxisName))
        {
            output = Input.GetAxis(kbAxisName);
            UsingKeyboard = true;
            button = true;
        } 
        else if (!IsAxisCloseEnoughToZero(conAxisName))
        {
            output = Input.GetAxis(conAxisName);
            UsingKeyboard = false;
            button = true;
        }
        else
        {
            output = 0f;
            button = false;
        }
        return output;
    }

    private float GetAxisInput(string kbAxisName, string conAxisName)
    {
        float output;
        if (!IsAxisCloseEnoughToZero(kbAxisName))
        {
            output = Input.GetAxis(kbAxisName);
            UsingKeyboard = true;
        }
        else if (!IsAxisCloseEnoughToZero(conAxisName))
        {
            output = Input.GetAxis(conAxisName);
            UsingKeyboard = false;
        }
        else
        {
            output = 0f;
        }
        return output;
    }

    private bool GetButtonDownInput(string kbButtonName, string conButtonName)
    {
        bool output;
        if (Input.GetButtonDown(kbButtonName))
        {
            output = true;
            UsingKeyboard = true;
        } 
        else if (Input.GetButtonDown(conButtonName))
        {
            output = true;
            UsingKeyboard = false;
        }
        else
        {
            output = false;
        }
        return output;
    }

    private bool GetButtonInput(string kbButtonName, string conButtonName)
    {
        bool output;
        if (Input.GetButton(kbButtonName))
        {
            output = true;
            UsingKeyboard = true;
        } 
        else if (Input.GetButton(conButtonName))
        {
            output = true;
            UsingKeyboard = false;
        }
        else
        {
            output = false;
        }
        return output;
    }

    private void GetButtonHoldAndDownInputs(string kbButtonName, string conButtonName, out bool buttonDown, out bool button)
    {
        buttonDown = GetButtonDownInput(kbButtonName, conButtonName);
        button = GetButtonInput(kbButtonName, conButtonName);
    }
    
    private void Update()
    {
        GrabPlayerInputs();
        GrabCameraInputs();
        GrabUiInputs();
    }
}
