﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCameraController : MonoBehaviour
{
    public Transform target;
    
    private const float MinDistance = 8f, MedDistance = 15f, MaxDistance = 25f;
    private readonly float[] zoomLevels = {MinDistance, MedDistance, MaxDistance};
    
    [Header("Zoom Levels")]
    [Range(MinDistance, MaxDistance)] public float distance;
    [SerializeField] [Range(0, 2)] private int selectedZoomLevel;
    public int SelectedZoomLevel
    {
        get => selectedZoomLevel;
        set => selectedZoomLevel = value < 0 ? zoomLevels.Length - 1 : value >= zoomLevels.Length ? 0 : value;
    }
    public float zoomingSpeed = 10f;
    
    [Header("Controls")]
    public Vector2 mouseSensitivity = Vector2.one;
    public Vector2 controllerSensitivity = Vector2.one;
    public Vector2 verticalRotationClamp = new Vector2(-90,90);

    private Vector3 targetRotation;
    private InputManager input;

    private void Start() 
    {
        input = InputManager.Instance;
        if (target == null)
            throw new NullReferenceException("TPC camera target is null.");
        
        targetRotation = transform.eulerAngles;
    }

    private void LateUpdate()
    {
        if (Time.timeScale == 0)
            return;

        CalculateDistanceToTarget();
    }
    
    private void CalculateDistanceToTarget()
    {
        SelectedZoomLevel += input.cameraZoomInput;
        distance = Mathf.Lerp(distance, zoomLevels[SelectedZoomLevel], Time.deltaTime * zoomingSpeed);
        //distance = Mathf.Clamp(distance, MinDistance, MaxDistance);
        
        if (input.cameraCenterButtonDown)
        {
            transform.position = target.position;
            transform.position -= transform.forward * distance;
            return;
        }
        
        var cameraInput = new Vector2(input.cameraHorizontalInput, input.cameraVerticalInput);
        cameraInput *= input.UsingKeyboard ? mouseSensitivity : controllerSensitivity;

        targetRotation.y += cameraInput.x;
        targetRotation.x -= cameraInput.y;
        targetRotation.x = Mathf.Clamp(targetRotation.x, verticalRotationClamp.x, verticalRotationClamp.y);

        transform.rotation = Quaternion.Euler(targetRotation);

        var newPosition = target.position - (transform.forward * distance);
        transform.position = newPosition;
    }
}
