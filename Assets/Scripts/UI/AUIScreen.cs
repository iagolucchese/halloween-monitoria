﻿using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(CanvasGroup))]
public abstract class AUIScreen : MonoBehaviour
{
    protected RectTransform rectTransform;
    protected CanvasGroup canvasGroup;

    [SerializeField] protected float tweenDurationMultiplier = 1f;
    [SerializeField] protected Vector3 onScreenPosition = Vector3.zero;
    [SerializeField] protected Vector3 offscreenPosition = Vector3.left * 1700f;
    
    protected virtual void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void TransitionOffScreen()
    {
        canvasGroup.interactable = false;
        ScaleBackThenMove(offscreenPosition);
    }

    public void TransitionIntoScreen()
    {
        canvasGroup.interactable = true;
        ScaleBackThenMove(onScreenPosition);
    }

    private void ScaleBackThenMove(Vector3 moveTo)
    {
        var sequence = DOTween.Sequence();
        sequence.Append(rectTransform.DOScale(Vector3.one * 0.9f, 0.2f * tweenDurationMultiplier));
        sequence.Append(rectTransform.DOAnchorPos(moveTo, 1.6f * tweenDurationMultiplier, true));
        sequence.Append(rectTransform.DOScale(Vector3.one, 0.2f * tweenDurationMultiplier));
    }
}
