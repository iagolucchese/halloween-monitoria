﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    private static HUDManager _instance;
    public static HUDManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogWarning("QuestHUDBehavior didnt exist on GET");
            }

            return _instance;
        }
    }

    public RawImage crosshair;
    
    private CanvasGroup canvasGroup;

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(gameObject);
        else
            _instance = this;

        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        crosshair.gameObject.SetActive(false);
        
        GlobalSceneControls.Instance.OnGamePaused += () => ToggleHUDElements(false);
        GlobalSceneControls.Instance.OnGameUnpaused += () => ToggleHUDElements(true);
        PlayerBehavior.Instance.OnAimingStart += () => crosshair.gameObject.SetActive(true);
        PlayerBehavior.Instance.OnAimingEnd += () => crosshair.gameObject.SetActive(false);
    }

    public void ToggleHUDElements(bool doShow)
    {
        canvasGroup.interactable = doShow;
        canvasGroup.alpha = doShow ? 1f : 0f;
    }
}
   
