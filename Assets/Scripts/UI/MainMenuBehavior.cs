﻿public class MainMenuBehavior : AUIScreen
{
    protected override void Awake()
    {
        base.Awake();
    }
    
    //button functions
    public void PlayGame()
    {
        GlobalSceneControls.GoToScene(1);
    }

    public void GoToCreditsScreen()
    {
        
    }

    public void GoToOptionsScreen()
    {
        
    }

    public void ExitGame()
    {
        GlobalSceneControls.ExitGame();
    }
}
