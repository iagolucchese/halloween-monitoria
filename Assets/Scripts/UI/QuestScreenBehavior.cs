﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SingleQuestHUDLine
{
    public readonly RectTransform questRect;
    public readonly Toggle questCheckmark;
    public readonly TextMeshProUGUI questLabel;
    //public RawImage crossedTextImage;

    public void ToggleCheckmark(bool isOn)
    {
        questCheckmark.isOn = isOn;
    }
    
    public SingleQuestHUDLine(RectTransform rect, Toggle checkmark, TextMeshProUGUI text)
    {
        questRect = rect;
        questCheckmark = checkmark;
        questLabel = text;
    }
}

public class QuestScreenBehavior : MonoBehaviour
{
    private const int MaxLinesOfText = 16;

    private static QuestScreenBehavior instance;
    public static QuestScreenBehavior Instance
    {
        get
        {
            if (instance != null) return instance;
            
            instance = FindObjectOfType<QuestScreenBehavior>();
            
            if (instance == null)
            {
                Debug.LogWarning("QuestHUDBehavior didnt exist on GET");
            }
            return instance;
        }
    }

    [SerializeField] private GameObject questLinePrefab;
    [SerializeField] private RectTransform questLinesParent;
    [SerializeField] private List<SingleQuestHUDLine> listOfQuestTextHuds;
    [SerializeField] private GameObject backButton;
    
    private QuestManager questManager;
    private HUDManager hudManager;
    private CanvasGroup canvasGroup;
    
    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else 
            instance = this;
        canvasGroup = GetComponent<CanvasGroup>();

        GenerateQuestHUDElements();
        /*listOfTaskLines = listOfTaskLines ?? new List<string>(MaxLinesOfText);
        allCheckmarks = allCheckmarks ?? new List<RectTransform>(MaxLinesOfText);
        tasksTextField = tasksTextField != null ? tasksTextField : GetComponentsInChildren<TextMeshProUGUI>().FirstOrDefault(x => x.IsActive());*/
    }
    
    private void Start()
    {
        questManager = QuestManager.Instance;
        ReloadQuestsFromManager();
        ToggleQuestHUDElements(false);

        GlobalSceneControls.Instance.OnGamePaused += () => ToggleQuestHUDElements(true);
        GlobalSceneControls.Instance.OnGameUnpaused += () => ToggleQuestHUDElements(false);
    }

    private void GenerateQuestHUDElements()
    {
        listOfQuestTextHuds = new List<SingleQuestHUDLine>(MaxLinesOfText);
        for (var i = 0; i < questLinesParent.childCount; i++)
        {
            var child = questLinesParent.GetChild(i);
            
            var newQtTextHud = new SingleQuestHUDLine(
                child.GetComponent<RectTransform>(), 
                child.GetComponentInChildren<Toggle>(), 
                child.GetComponentInChildren<TextMeshProUGUI>()
            );
            listOfQuestTextHuds.Add(newQtTextHud);
        }
    }
    
    public void ToggleQuestHUDElements(bool show)
    {
        canvasGroup.interactable = show;
        canvasGroup.alpha = show ? 1f : 0f;
        if (show)
        {
            ReloadQuestsFromManager();
            if (backButton != null)
                EventSystem.current.SetSelectedGameObject(backButton);
        }
    }

    private void ReloadQuestsFromManager()
    {
        var list = new List<QuestObjective>();
        foreach (var quest in questManager.ActiveQuestList)
        {
            list.AddRange(quest.objectives);
        }
        
        if (list.Count >= MaxLinesOfText)
            Debug.LogWarning("There's more total objectives active than there are lines of text in the Quest HUD! HUD has " + 
                             MaxLinesOfText + " lines, and there are " + list.Count + " objectives");
        
        for (var i = 0; i < MaxLinesOfText; i++)
        {
            var textHud = listOfQuestTextHuds[i];

            if (i >= list.Count)
            {
                textHud.questRect.gameObject.SetActive(false);
                continue;
            }
            textHud.questRect.gameObject.SetActive(true);
            
            var objective = list[i];
            textHud.questLabel.text = objective.objectiveDescription;
            if (objective.objectiveType == QuestObjective.ObjectiveTypes.SimpleCounter)
            {
                textHud.questLabel.text += ": " + objective.CounterCurrentProgress + " / " + objective.CounterGoal;
            }
            textHud.ToggleCheckmark(objective.ObjectiveHasCompleted);
        }
    }

    public void BackButtonAction()
    {
        GlobalSceneControls.Instance.HandlePausing(false);
    }
}
