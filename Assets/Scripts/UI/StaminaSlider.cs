﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class StaminaSlider : MonoBehaviour
{
    private Slider slider;
    private PlayerMovementController pc;

    void Start()
    {
        slider = GetComponent<Slider>();
        pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovementController>();
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = pc.stamina;
    }
}
