﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSelectHUD : MonoBehaviour
{
    #pragma warning disable 0649

    public Texture2D selectedSlot;
    public Texture2D unselectedSlot;
    public RawImage selectedCenterImage;
    
    private int selectedWeaponHud;
    [SerializeField] private List<RawImage> slotList;
    [SerializeField] private List<RawImage> slotIconList;

    private PlayerBehavior playerBehavior;
    
    private void OnEnable()
    {
        slotList = slotList ?? new List<RawImage>();
        playerBehavior = PlayerBehavior.Instance;
        playerBehavior.OnWeaponChanged += SelectedWeaponChanged;

        SetSlotIconsFromWeaponList();
        UpdateHUD();
    }

    private void SetSlotIconsFromWeaponList()
    {
        //var weapons = playerBehavior.listOfWeapons;
        var weapons = WeaponList.GetInstance().allWeapons;
        
        for (var i = 0; i < slotList.Count; i++)
        {
            if (i >= weapons.Count)
            {
                break;
            }
            slotIconList[i].color = Color.white;
            slotIconList[i].texture = weapons[i].iconInHud;
        }
    }

    private void SelectedWeaponChanged(int newWeapon)
    {
        selectedWeaponHud = newWeapon;
        UpdateHUD();
    }

    private void UpdateHUD()
    {
        var weapons = playerBehavior.listOfWeapons;
        for (var i = 0; i < slotList.Count; i++)
        {
            if (selectedWeaponHud == i)
            {
                slotList[i].texture = selectedSlot;
                if (i >= weapons.Count)
                {
                    Debug.LogWarning("Weapon HUD: selected weapon number bigger than the size of the list of weapons asset.");
                    continue;
                }
                selectedCenterImage.texture = weapons[i].iconInHud;
            }
            else
                slotList[i].texture = unselectedSlot;
        }
    }
}
