﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = DefaultFilename, menuName = "Weapons/New Weapon")]
public class Weapon : ScriptableObject
{
    public const string DefaultFilename = "NewWeapon";
    
    public string weaponName = "_DEFAULT";
    public Texture2D iconInHud;
    public GameObject projectilePrefab;
    public int ammoLimit = -1;
    public Vector3 throwForce = Vector3.forward;
}