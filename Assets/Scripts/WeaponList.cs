﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = DefaultFilename, menuName = "Weapons/Weapon List")]
public class WeaponList : ScriptableObject
{
    public const string DefaultFilename = "WeaponList";
    public const string DefaultFilepath = "WeaponAssets/WeaponList";
    public List<Weapon> allWeapons;
    
    public static WeaponList GetInstance()
    {
        WeaponList list;
        list = Resources.Load<WeaponList>(DefaultFilepath);
        
        if (list == null)
            list = Resources.FindObjectsOfTypeAll<WeaponList>().FirstOrDefault();
        
        return list;
    }
}
