﻿using UnityEngine;

public class WeaponProjectile : MonoBehaviour
{
    public float destroyAfterSeconds = -1f;
    private new Rigidbody rigidbody;
    private new Collider collider;
    
    private void Awake()
    {
        rigidbody = GetComponentInChildren<Rigidbody>();
        collider = GetComponent<Collider>();
        if (collider == null)
            Debug.LogError(gameObject.name + " is a weapon projectile item without a collider");
        
        if (destroyAfterSeconds > 0)
            Destroy(gameObject, destroyAfterSeconds);
    }

    public void Throw(Vector3 throwForce, Vector3 forwardDirection)
    {
        transform.parent = null;
        TogglePhysicsAndInteraction(true);
    
        //Physics.IgnoreLayerCollision(gameObject.layer, PlayerBehavior.Instance.gameObject.layer);
        Physics.IgnoreCollision(gameObject.GetComponent<Collider>(), PlayerBehavior.Instance.GetComponent<Collider>(), true);
    
        rigidbody.AddForce(
            throwForce.z * forwardDirection, 
            ForceMode.Force
        );
        rigidbody.AddForce(
            throwForce.y * Vector3.up, 
            ForceMode.Force
        );
    }
    
    private void TogglePhysicsAndInteraction(bool on)
    {
        rigidbody.useGravity = on;
        rigidbody.isKinematic = !on;
        collider.enabled = on;
    }
}
